/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.WriteException;
/**
 *
 * @author Gleb
 */
public class TimeTableAnalyzer
{
    
    public static void main(String[] args) 
            throws IOException, WriteException 
  {
		TimeTable tt = new TimeTable();
        // open .xls schedules
        LinkedList<Workbook> workbooks = tt.OpenWorkbooks();
        Iterator<Workbook> wbit = workbooks.iterator();
       
        //open each sheet and load schedule in memory
        while(wbit.hasNext())
        {
           Workbook curW =  (Workbook)wbit.next();
           LinkedList<Sheet> sheets = tt.OpenSheet(curW);
           Iterator<Sheet> sit = sheets.iterator();
           while(sit.hasNext())
           {
               tt.LoadTimetables((Sheet)sit.next(), tt.busywk);
           }
           curW.close(); 
       }
       //write list of all classrooms on campus
       tt.WriteAllClassrooms();
       //search schedule for free and open classrooms
       try
            {
                tt.SearchFreeClassrooms(tt.busywk, tt.freewk);
            }
       catch(BiffException empty)
            {
                System.out.println("\nBiffException thrown\n");
            }
       //write free and open classrooms schedule to .xls file
       tt.WriteFreeClassrooms(tt.freewk, tt.busywk);
       
       //report stats.
       System.out.printf("\n\nSchedule analyzed successfully.\n\n"
               + "Classes per week: %d\n"
               + "Free classrooms per week: %d\n"
               + "Free, but locked classrooms per week: %d\n"
               + "Classrooms options per week: %d\n"
               + "Potential Errors: %d\n"
               + "See FreeClassrooms.xls for results.\n", tt.countclasses, tt.countfreerooms, 
               tt.countfreelocked, (7*8*tt.allclassrooms.size()),
               ((tt.countfreerooms+tt.countclasses+tt.countfreelocked)
               - 7*8*tt.allclassrooms.size()));
       
       //Upload results to database
       Uploader uploader = new Uploader(tt.freewk);
       uploader.upload();
       
       //close all .xls files used
       while(wbit.hasNext())
       {
           Workbook wbk = (Workbook)wbit.next();
           wbk.close();
       }  
	}
  
}
