/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


public class Classroom
   {
       String name;
       private java.lang.Boolean open;
       
       public Classroom(String crname)
               {
                   this.name = crname;
                   this.open = false;
               }
       public String GetName()
       {
           return this.name;
       }
       public java.lang.Boolean OpenClassroom()
       {
           this.open = true;
           return true;
       }
       public java.lang.Boolean IsOpen()
       {
           return this.open;
       }
       public java.lang.Boolean CloseClassroom()
       {
           this.open = false;
           return true;
       }          
   }