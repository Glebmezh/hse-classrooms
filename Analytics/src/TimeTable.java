/*
 * TimeTable 
 * by Gleb Mezhanskiy
 * API used: Java Excel API http://jexcelapi.sourceforge.net
 * 
 * Description:
 * TimeTable analyzes .xls class schedules from Input folder and creates
 * a schedule of unoccupied(free) and open classrooms available for students.
 * 
 * The programm assumes that all clasrooms remain locked at the beginning of each day
 * until opened by a faculty who is the first to give a class in the room. The room
 * remains open then until the 8th session ends.
 * 
 * Tutorial:
 * 1) Name all .xls files in format *.xls, where * is an integer
 * consecutively starting with 0. e.g. 0.xls, 1.xls etc.
 * 2) Make sure each day in timetables has exactly 8 sessions. 
 * Insert extra rows if necessary.
 * Delete redundunt rows if they exist.
 * 3) Place all .xls input files in Input folder
 * 4) Run the programm
 * 5) Find the analyzed timeTable in FreeClassrooms.xls file.
 * 
 * Hints:
 * 
 * 1) To produce valid results all timetables on campus must me loaded. The number
 * of actually busy rooms listed as free grows as more schedules are omitted.
 * 2) Pay attention to input .xls formatting. 
 *          1) Make sure each day in timetables has 8 sessions.
 *             Insert extra rows if necessary.
 *          2) Make sure each column that contains rooms numbers
 *             has a header exactly one row above.   
 * 
 */


import java.io.File; 
import jxl.*;
import java.io.IOException;
import jxl.read.biff.BiffException;
import java.util.LinkedList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.*;
import jxl.write.*; 

public class TimeTable {
    public static Scanner userIn = new Scanner(System.in);  
    
    LinkedList<Classroom> allclassrooms = new LinkedList<Classroom>();  
    int countclasses = 0;
    int countfreerooms = 0;
    int countfreelocked = 0;

    //original schedule
    Week busywk = new Week();
    //free classrooms schedule
    Week freewk = new Week();
        
   

public void CollectClassrooms(Classroom inclassroom) throws IOException
// The function collects all classrooms from original schedule to form a list of
// all classrooms on campus.      
    {   
        Iterator<Classroom> it = allclassrooms.listIterator();
        java.lang.Boolean classroomExists = false;
        while(it.hasNext())
        {
            Classroom listclassroom = (Classroom)it.next();
            String lcname = listclassroom.GetName();
            //check if the classroom is already in the list.
            if(lcname.equals(inclassroom.GetName()))
            {
                classroomExists = true;
                break;
            }
        }
        if(!classroomExists)
        {
           allclassrooms.add(inclassroom);
        }
       
    }
    
public void WriteAllClassrooms() throws IOException, WriteException
// writes all classrooms on campus to .xls file        
    {
        WritableWorkbook wwb = Workbook.createWorkbook(new File("Results/classrooms.xls"));
        WritableSheet wsheet = wwb.createSheet("ClassroomsList", 0);
        int wcrow = 0;
        Iterator<Classroom> it = allclassrooms.listIterator();        
        
        while(it.hasNext())
        {
        Classroom clptr = (Classroom)it.next();  
        String classname = clptr.GetName();
        jxl.write.Label classlabel = new jxl.write.Label(0, wcrow, classname); 
        wsheet.addCell(classlabel);
        wcrow++;
        }     
        wwb.write(); 
        wwb.close();     
    }

public void LoadTimetables(Sheet sheet, Week inwk)      
        throws IOException, ArrayIndexOutOfBoundsException
//loads original classes timetables in memory. 
{
        Pattern pat = Pattern.compile("((\\d)(\\d)(\\d)(\\d))");
        Pattern cornerPattern = Pattern.compile("9.00-10.20");
        //Pattern groupn = Pattern.compile("((\\d)(\\d)(\\d)(\\d))");
        
        int row = 0;
        int groups = 0;
        // find the beginning of the timetable in spreadhseet
        while(row < 15)
        {        
              String cellcont = this.ReadCell(sheet, 1, row);       
              Matcher cornmat = cornerPattern.matcher(cellcont);
                    if(cornmat.find())
                    {
                        break;
                    }
                   row++;       
        }
        // find number of columns to read
        while(groups < 20)
        {
            String cellcont = this.ReadCell(sheet, groups + 2, row - 1);                          
            if(!cellcont.isEmpty())
            {     
                groups++;
            }
            else break;
        }
        // iterate through weekdays
        for(int i = 0; i < 7; i++)
        { 
            Day curDay = (Day)inwk.days[i];
            //iterate through sessions on a given day
            for(int session = 0; session < 8; session++)
            {   
                ClassroomList classline = curDay.sessions[session];
                //iterate through groups (sections) on a given session
                for(int column = 2; column < groups + 2; column ++)
                {   
                    String cellcont = this.ReadCell(sheet, column, row);
                    Matcher mat = pat.matcher(cellcont);
                        
                    while(mat.find())
                    {
                    	String foundName = mat.group();
                    	if(!classline.isInSession(foundName))
                    	{
                        Classroom classroom = new Classroom(foundName);
                        
                        //add classroom to the list in schedule loaded to memory    
                        classline.add(classroom);
                        CollectClassrooms(classroom);
                        curDay.busyClassroomsStat++;
                        this.countclasses ++;
                        } 
                    }
                }
                row++;
            }
         }
 }

public void SearchFreeClassrooms(Week ibwk, Week ifwk) 
        throws IOException, BiffException
//Checks each classroom from all classrooms list on whether it is free and open
//during each session. Loads free classrooms HashMap in memory.
{ 
    
//find free classrooms for each session on each day
    for(int fweekday = 0; fweekday < 7; fweekday++)
     { 
       Day curFDay = (Day)ifwk.days[fweekday];
       Iterator<Classroom> fcit = allclassrooms.iterator();
       
       while(fcit.hasNext())
       {
           Classroom cr = (Classroom)fcit.next();
           cr.CloseClassroom();
       }
       
       Day curBDay = (Day)ibwk.days[fweekday];
       
       curFDay.busyClassroomsStat = curBDay.busyClassroomsStat;
       
         for(int fsession = 0; fsession < 8; fsession++)
           {  
             //create free classrooms list to write  
             ClassroomList freeclassline = curFDay.sessions[fsession];
             // open busy classrooms list
             ClassroomList bclassline =  curBDay.sessions[fsession];
             
//check if the classroom is free for a specific session on a specific day
    
             fcit = allclassrooms.iterator();
             
             java.lang.Boolean freeclassroom;
             
             //iterate through all classrooms list
             while(fcit.hasNext())
             {
               // take one classroom from all classrooms list
               Classroom clptr = (Classroom)fcit.next();
               String classroom = clptr.GetName();     
               freeclassroom = true; 
               
               //check if the current classroom from all classrooms list is busy. If it is busy, set it as opened for this day.
               if(bclassline.isInSession(classroom))
               {
                   clptr.OpenClassroom();
                   freeclassroom = false;
               }
               
               //check if the classroom was open today
               if(freeclassroom && !clptr.IsOpen())
               {
                   this.countfreelocked ++;
                   curFDay.freeLockedClassroomsIncrement();
               }
               //if classroom is free and open add to the list
               if(freeclassroom && clptr.IsOpen() == true)
               {
                     freeclassline.add(clptr);
                     this.countfreerooms++;
                     curFDay.freeClassroomsIncrement();
               }    
              }   
            }
        }
  }
    
public void WriteFreeClassrooms(Week fwk, Week bwk) 
        throws IOException, WriteException
//writes free and open classrooms in .xls timetable file.        
{
   WritableWorkbook wwb = Workbook.createWorkbook(new File("Results/FreeClassrooms.xls"));
   WritableSheet wsheet = wwb.createSheet("ClassroomsList", 0);
   int session = 0;
   int sesptr = 0;
   for(int fweekday = 0; fweekday < 7; fweekday++)
     { 
        // print weekdays 
        int dayposition = fweekday + sesptr * 8;
        jxl.write.Label day = new jxl.write.Label(0,dayposition,
                fwk.weekdayslist[fweekday]); 
        wsheet.addCell(day);
        
        Day curFDay = (Day)fwk.days[fweekday];

        /* DEbugging block
        jxl.write.Number stats = new jxl.write.Number(6, dayposition,
                curFDay.busyClassroomsStat); 
        wsheet.addCell(stats);
        stats = new jxl.write.Number(7, dayposition,
                curFDay.freeClassroomsStat); 
        wsheet.addCell(stats);
        stats = new jxl.write.Number(8, dayposition,
                curFDay.freeLockedClassroomsStat); 
        wsheet.addCell(stats);
        stats = new jxl.write.Number(9, dayposition,
                (allclassrooms.size()*8 -curFDay.busyClassroomsStat-curFDay.freeClassroomsStat-curFDay.freeLockedClassroomsStat)); 
        wsheet.addCell(stats);
       */
        
        sesptr++;
        for(session = 0; session < 8; session++)
        {   
         //print sessions 
         jxl.write.Label ps = new jxl.write.Label(1,(dayposition + session),
                curFDay.sessionslist[session]); 
         wsheet.addCell(ps);
         
         ClassroomList freeclassline = curFDay.sessions[session];
         
         //print free classrooms for each session
         
         String freeclassroomslist = "";
         ClassroomListIterator cli = new ClassroomListIterator(freeclassline);
         Classroom curcl = cli.getClassroom();
 
         while(!(curcl == null))
         {            
            freeclassroomslist += (curcl.GetName() + "; ");
            curcl = cli.getClassroom();
         }
         jxl.write.Label classroomsstring = 
                 new jxl.write.Label(2,(dayposition + session), freeclassroomslist);   
         wsheet.addCell(classroomsstring);
         
         }
      }
   		//close all workbooks
        wwb.write(); 
        wwb.close();  
  } 

public LinkedList<Sheet> OpenSheet(Workbook wbk)
//takes workbook and returns all non-empty sheets        
{
    int i = 0;
    LinkedList<Sheet> sheets = new LinkedList<Sheet>();
    while(true)
    {
        try
    {
        sheets.add(wbk.getSheet(i));
        i++;
    }
    catch(java.lang.IndexOutOfBoundsException jlaioube)
    {
        return sheets;
    }    
    }
 }
    
public LinkedList<Workbook> OpenWorkbooks()
//Opnes all .xls workbooks in root directory        
    {
        int i = 0;
        LinkedList<Workbook> workbooks = new LinkedList<Workbook>();
        while(true)
        try 
        {
            workbooks.add(Workbook.getWorkbook(new File("Input/" + i + ".xls")));
            i++;
        }
        catch(IOException iox)
        {
            return workbooks;
        }
        catch(BiffException bx)
        {
            return workbooks;
        } 
    }

 public String ReadCell(Sheet sht, int col,int r)
 //reads spreadsheet cells and handles errors.        
  {
    try
    {
        Cell rcell = sht.getCell(col, r);
        String rcellString = rcell.getContents();
        return rcellString;
    }
    catch(java.lang.ArrayIndexOutOfBoundsException jaoub)
    {
        return "";
    }
  }    
} 



   