/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gleb
 */
public class Day
        {
            ClassroomList[] sessions = new ClassroomList[9];
            private String day; 
            int freeClassroomsStat = 0;
            int freeLockedClassroomsStat = 0;
            int busyClassroomsStat = 0;
            public final String[] sessionslist = {"9.00-10.20", "10.30-11.50", 
        "12.10 - 13.30", "13.40 - 15.00", "15.10 - 16.30", "16.40 - 18.00", 
        "18.10 - 19.30", "19:40-21:00"};
            
            public Day(String day)
            {
                this.day = day;   
                for(int i = 0; i < 8; i++)
                {
                    ClassroomList classes = new ClassroomList();
                    this.sessions[i] = classes;
                }
            }
            
        public int freeLockedClassroomsIncrement()
        {
            this.freeLockedClassroomsStat++;
            return freeLockedClassroomsStat;
        }
        
        public int getFreeLockedClassroomsStat()
        {
            return this.freeLockedClassroomsStat;
        }
        
         public int freeClassroomsIncrement()
        {
            this.freeClassroomsStat++;
            return freeClassroomsStat;
        }
        
        public int getFreeClassroomsStat()
        {
            return this.freeClassroomsStat;
        }

        }