import java.awt.EventQueue;
import java.awt.Toolkit;
import javax.swing.*;
import java.awt.*;
import javax.swing.JFrame;
import java.awt.event.*;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


public class TimeTableGUI
   {
     public static void main(String[] args)

     {
    	 EventQueue.invokeLater(new Runnable()
    	 
    	 {
    		 public void run()
    		 {
    			 SimpleFrame frame = new SimpleFrame();
    			 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    			 frame.setVisible(true);
    			 
    		 }
    	 });    	 
     }
   }
class SimpleFrame extends JFrame
{
	private JPanel buttonPanel;
	public SimpleFrame() 
	
	{
		
	//get user's screen dimensions
	Toolkit kit = Toolkit.getDefaultToolkit();
	Dimension screenSize = kit.getScreenSize();
	int screenHeight = screenSize.height;
	int screenWidth = screenSize.width;
	// set window size half each screen dimension.
	setSize(screenWidth/2, screenHeight/2);
	
	setTitle("TimeTable");
		
	
	JButton start = new JButton("Start");
	buttonPanel = new JPanel();
	
	buttonPanel.add(start);
	add(buttonPanel);
	
	StartAction startBtnAction = new StartAction(Color.BLUE);
	start.addActionListener(startBtnAction);
	
	

	}
	
	class StartAction implements ActionListener
	{
		private Color backgroundColor;
		public StartAction(Color c)
		{
			backgroundColor = c;
			
		}
		
		public void actionPerformed(ActionEvent event)
		{
			buttonPanel.setBackground(backgroundColor);
			
		}
	}
	
	public static final int DEFAULT_WIDTH = 500;
	public static final int DEFAULT_HEIGHT = 200;
	
}