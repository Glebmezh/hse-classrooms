/*Iterates Classrooms TreeMap
 * 
 * 
 */

import java.util.NoSuchElementException;


public class ClassroomListIterator {

	ClassroomList classroomList;
	private String iteratorkey;
	public ClassroomListIterator(ClassroomList cll)
	{
		classroomList = cll;
		iteratorkey = "";
	}
	
	//returns classroom next in the list in ascending order or null if no classroom is to return
	public Classroom getClassroom()
    {
   	 try
        {
   		 if(iteratorkey.equals(""))
   		 {	 
   			 try
   			 {
   			 iteratorkey = classroomList.firstKey().toString();
   			 return (Classroom)classroomList.get(iteratorkey);
   			 }
   			 catch (NullPointerException npe)
   			 {
   				 return null;
   			 }
   		 }
   		 else
   		 {	 
   			 try
   			 {
   			 iteratorkey = classroomList.higherKey(iteratorkey).toString();
   			 return (Classroom)classroomList.get(iteratorkey);
   			 }
   			 catch (NullPointerException npe)
   			 {
   				 return null;
   			 }
   		 }
        }
       catch (NoSuchElementException nsee)
       {
       	return null;
       }
    }

}
