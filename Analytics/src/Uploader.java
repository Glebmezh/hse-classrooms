import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

import org.apache.http.Consts;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import com.sun.org.apache.xerces.internal.util.URI;


//Uploads timetable to remote mySQL database via POST request to PHP
public class Uploader {
	static Scanner userIn = new Scanner(System.in);
	private Week timetable;

	public Uploader(Week w)
	{
		timetable = w;
	}

	public Boolean upload() throws IOException
	{
		System.out.println("Preparing query...");
		//String for mySQL INSERT query
		String values = "";
		//for each day
		for(int i = 0; i < 7; i ++)
		{
			//for each session
			for(int j = 0; j < 8; j ++)
			{
				ClassroomList cll = timetable.days[i].sessions[j];
				ClassroomListIterator cli = new ClassroomListIterator(cll);
				Classroom clrm = cli.getClassroom();
				while(clrm != null)
				{
					values += "(" + i + ", " + j +", " + clrm.GetName() + "), ";
					clrm = cli.getClassroom();
				}	
			}
		}
		values = values.substring(0, values.length() - 2);
		//System.out.println(query);
		executeRequest(values);
		return true;
	}
	
	public boolean executeRequest(String q) throws IOException
	{
    	//System.out.println("Enter query password...");
    	//String password = userIn.next();
		CloseableHttpClient client = HttpClients.createDefault();
    	System.out.println(q);
    	
    	 try {
    		 
    		 	ArrayList<NameValuePair> formparams = new ArrayList<NameValuePair>();
    		 	formparams.add(new BasicNameValuePair("type", "Dataload"));
    		 	formparams.add(new BasicNameValuePair("values", q));
    		 	UrlEncodedFormEntity entity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
    		 
    	        HttpPost request = new HttpPost("http://timetable.31f.org/backend.php");
    	        //StringEntity params = new StringEntity(q);
    	        //params.setContentType("application/json")
    	        
    	        request.addHeader("content-type", "application/x-www-form-urlencoded");
    	        request.setEntity(entity);
    	        System.out.println("Executing query.....");
    	        HttpResponse response = client.execute(request);
    	        
    	        BufferedReader  rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
    	        String line = "";
    	          while ((line = rd.readLine()) != null) {
    	            System.out.println(line);
    	          }

    	        } 
    	 catch (IOException e) {
    	          e.printStackTrace();
    	        }

   
    	 catch (Exception ex) {

    	    } 
    	 
    	 finally {
    	        client.close();
    	        return true;
    	    }
	}
}
