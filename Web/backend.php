<?php 

//database connection:
//open connection to database
$dbcnx = @mysql_connect("localhost", "org31f_timetable", "polka2013");
if (!$dbcnx) {
  echo( "<P>Unable to connect to the " .
        "database server at this time.</P>" );
  exit();
}

//select DB to work with

if (! @mysql_select_db("org31f_timetable") ) {
  echo( "<P>Unable to locate the " .
        "database at this time.</P>" );
  exit();
}


if(!empty($_POST))
{
// SELECT query
if($_POST['type'] == "SELECT FEED")
{

$sql = 'SELECT DISTINCT t.No, Classroom FROM Timetable t LEFT OUTER JOIN Feedback f ON t.No = f.No WHERE Weekday = ' . $_POST['Weekday'] . ' AND Session = ' . $_POST['Session'] . ' AND t.No NOT IN(SELECT DISTINCT t.No FROM Timetable t LEFT OUTER JOIN Feedback f ON t.No = f.No WHERE Weekday = ' . $_POST['Weekday'] . ' AND Session = ' . $_POST['Session'] . ' AND (CURDATE() = DATE(Posted)))';

$sql_results = mysql_query($sql);

//check for error
if (!$sql_results) {
  echo("<P>Error performing query: " .
       mysql_error() . "</P>");
  exit();
}

//encode to JSON
$rows = array();
while($r = mysql_fetch_assoc($sql_results)) 
{
    $rows[] = $r;
}

$jresult = json_encode($rows);

echo $jresult;
}
else if($_POST['type'] == "Feedback")
{

	 $no = $_POST['no'];
	 $feedback = $_POST['feedback'];
	 $weekday = $_POST['weekday'];
	 $session = $_POST['session'];
	
// INSERT query
$sql = 'INSERT INTO Feedback (No, Feedback) VALUES (\''. $no . '\', \'' . $feedback . '\')';

	if (mysql_query($sql)) {
    echo(mysql_insert_id());
	} 
	else {
    echo("<P>Error inserting feedback. " .
         mysql_error() . "</P>");
	}
	
}
// INSERT query based on data sent by analyzer Java app
else if($_POST['type'] == "Dataload")
{
echo($_POST['values']);
// INSERT query
$sql = 'INSERT INTO Timetable (Weekday, Session, Classroom) VALUES ' . $_POST['values'];
if (mysql_query($sql)) {
    echo("   Timetable loaded successfully");
  } else {
    echo("   Error adding submitted proposition: " .
         mysql_error());
  }
}
else echo("Error: Unknown request.");

}

//Returns session number that is closest to current time
function getSession()
{
	//Get current time
	date_default_timezone_set('Europe/Moscow');
	$cursession = 0;
	$curtime = date('H:i:s');
	
	//Determine which session is the closest
		 if (strtotime($curtime) >= strtotime("09:00:00") && strtotime($curtime) < strtotime("10:20:00")) {$cursession = 0;}
	else if (strtotime($curtime) >= strtotime("10:20:00") && strtotime($curtime) < strtotime("11:50:00")) {$cursession = 1;}
	else if (strtotime($curtime) >= strtotime("11:50:00") && strtotime($curtime) < strtotime("13:30:00")) {$cursession = 2;}
	else if (strtotime($curtime) >= strtotime("13:30:00") && strtotime($curtime) < strtotime("15:00:00")) {$cursession = 3;}
	else if (strtotime($curtime) >= strtotime("15:00:00") && strtotime($curtime) < strtotime("16:30:00")) {$cursession = 4;}
	else if (strtotime($curtime) >= strtotime("16:30:00") && strtotime($curtime) < strtotime("18:00:00")) {$cursession = 5;}
	else if (strtotime($curtime) >= strtotime("18:00:00") && strtotime($curtime) < strtotime("19:20:00")) {$cursession = 6;}
	else if (strtotime($curtime) >= strtotime("19:20:00") && strtotime($curtime) < strtotime("22:00:00")) {$cursession = 7;}
	else {$cursession = 0;}
	
	return $cursession;
}

//Returns current day of the week
function getWeekday()
{
	date_default_timezone_set('Europe/Moscow');
	return date('N');
}
?>