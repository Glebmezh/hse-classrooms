/***
Performs dynamic page load, database queries by sending requests to PHP.
***/			

//Gets list of classrooms from database
//Then loads them to HTML page
function getFeed(weekday, session, flang)
    {
    	//show loading widget while loading content
    	$.mobile.loading('show');
    	//MySQL query
    	var query = '{"type" : "SELECT FEED",'+
                          '"Weekday" : "'+weekday+'", "Session" : "'+session+'"}';
		//encode to JSON
		var jsonquery = $.parseJSON(query);
			
        //execute query and populate classrooms list
        $.post( "backend.php", jsonquery,
        	function( data )
            {		  	
            
            	
            	if(data.length > 0)
				{
                //clear the old classroom list
                $('#feedcontent div').remove();
                $('#listheader').show();
                $('#emptymsg').hide();
		  
				// for each classroom in database generate HTML code for list element (<li></li>) and append it to the feedlist DOM object  
                for(var i =0; i < data.length; i ++)
                {
                //create string oh HTML code               
					if(flang == "en")
					{
						var str = '<div id="'+data[i].No+'" data-role="collapsible" data-collapsed-icon="flat-eye" data-expanded-icon="flat-cross"'+
				 		  'data-collapsed="true" data-theme="a"><h3>'+data[i].Classroom+'</h3><p>Your feedback is crucial to enhance accuracy!</p>'+
				 		  '<p>Is the classroom vacant?</p>'+
				 		  '<fieldset class="ui-grid-a">'+
				 		  '<div class="ui-block-c"><button feedback="right" no="'+data[i].No+'" class = "" data-icon="flat-heart" data-theme="b">'+
				 		  'Yes, it\'s free! </button></div>'+
				 		  '<div class="ui-block-c"><button feedback="wrong" no="'+data[i].No+'" class = "" data-icon="flat-cross" data-theme="d">'+
				 		  'No, it\'s busy :(</button></div></fieldset></div>';
				 	}
				 	else if(flang == "rus")
				 	{
					 	var str = '<div id="'+data[i].No+'" data-role="collapsible" data-collapsed-icon="flat-eye" data-expanded-icon="flat-cross"'+
				 		  'data-collapsed="true" data-theme="a"><h3>'+data[i].Classroom+'</h3><p></p>'+
				 		  '<p>Аудитория действительно свободна?</p>'+
				 		  '<fieldset class="ui-grid-a">'+
				 		  '<div class="ui-block-c"><button feedback="right" no="'+data[i].No+'" class = "" data-icon="flat-heart" data-theme="b">'+
				 		  'Да, свободна! </button></div>'+
				 		  '<div class="ui-block-c"><button feedback="wrong" no="'+data[i].No+'" class = "" data-icon="flat-cross" data-theme="d">'+
				 		  'Нет, тут пара :(</button></div></fieldset></div>';	
				 	} 		  
				 		 
                //convert the string into actual HTML
                	var htmlstr = $.parseHTML(str);
                            
                //append the list
                	$('#feedcontent').append(htmlstr);
                //update the element so it's style is properly shown 
                	$('#'+data[i].No).trigger('create');
                }
                
                //update the list
                $('#feedcontent').collapsibleset();
                //hide loading widget
                $.mobile.loading('hide');                   
				//if no classrooms retrieved from database
				}
				else
                {
                //clear the list and show the message
	            $('#feedcontent div').remove();
	            $('#listheader').hide();
	            $('#emptymsg').show();
	            $.mobile.loading('hide');
                }
             //end of callback function   
             },"json");
	 }
//calls getFeed with user-specified parameters, handles feebdack.

function templateLoader()
{
	$('#feed').bind('pageinit', function()
	{	
			$.get("rus.html", function(data)
			{
				$("#feedheader").html(data).trigger('create');
				feedManager("rus");
			});
		
		$( "#lang" ).on( 'slidestop', function(event)
		{
			var lang = $("#lang").val();
			$.get(lang+".html", function(data)
			{
				$('#feedheader div').remove();
				$("#feedheader").html(data).trigger('create');
				feedManager(lang);
			});
			
		});	
	});
}
function feedManager(lang)
	{	
		var weekday;
		var session;

			
		//get from the filter form
		weekday = $("#weekday").val();
		session = $("#session").val();
		//load feed
		getFeed(weekday, session, lang); 
	
			//when user submits filter form
			$("#search").click(function()
			{
	        	weekday = $("#weekday").val();
				session = $("#session").val();
				
				//send Google Analytics event
				ga('send', 'event', 'search', 'action', {'metric1': weekday, 'metric2': session});
				
				//load feed
				getFeed(weekday, session, lang); 	        
			});
			
			//when user gives feedback for a classroom in the list
			$(document).on('click', '[feedback="right"]', function()
			{	
				//send Google Analytics event
				ga('send', 'event', 'classroom', 'right');
				
				//Record feedback to database
	        	feedbackRecorder(1, $(this).attr("no"),lang);
				$("button[no='"+$(this).attr("no")+"']").attr('class','ui-disabled');
				$('#'+$(this).attr("no")).trigger('create');        
			});
        
			$(document).on('click', '[feedback="wrong"]', function()
			{
				//send Google Analytics event
				ga('send', 'event', 'classroom', 'wrong');
				
				//Record feedback to database
	        	feedbackRecorder(-1, $(this).attr("no"), lang);
				$("button[no='"+$(this).attr("no")+"']").attr('class','ui-disabled');
				$('#'+$(this).attr("no")).trigger('create'); 	        
			});
			
			$("#infopage").click(function()
			{	
				//send Google Analytics event
				ga('send', 'event', 'infopage', 'view');	
			});
    }

//Records user's feedback in database        
function feedbackRecorder(type, no, lang)
	{
		var query = '{"type" : "Feedback", '+
                          '"no" : "'+no+'", "feedback" : "'+type+'"}';
                          
		var jsonquery = $.parseJSON(query);
		$.post( "backend.php", jsonquery,
            function( data )
            {
            	if(lang = "en")
            	{
                alert("Thank you for your feedback!");
                }
                else alert("Спасибо!");
            },  "json");
    }         


	
	
	
	
	
	
	
	
